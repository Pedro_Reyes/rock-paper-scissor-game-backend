package com.game.rock_paper_scissors.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import com.game.rock_paper_scissors.model.GlobalGameReport;
import com.game.rock_paper_scissors.model.LocalGameReport;
import com.game.rock_paper_scissors.model.EnumPlayerMovement;
import com.game.rock_paper_scissors.model.Round;
import com.game.rock_paper_scissors.model.EnumRoundResult;
import com.game.rock_paper_scissors.model.User;

public class GameUtilTest {

	private static String USER_1 = "1";
	private static String USER_2 = "2";
	private static String USER_3= "3";
	private static String USER_4 = "4";

	private static final long NUMBER_OF_GENERATED_UID = 10L;
	private static final long NUMBER_OF_GENERATED_ROUNDS = 10L;
	
	@Test
	public void shouldGenerateUIDNotRepeated() {
		List<String> uids = Stream.generate(() -> GameUtil.generateNewUID()).limit(NUMBER_OF_GENERATED_UID)
				.collect(Collectors.toList());

		// Assert
		assertThat(uids.stream().distinct().count(), is(NUMBER_OF_GENERATED_UID));
	}

	@Test
	public void shouldSecondPlayerAlwaysPlayRock() {
		assertThat(new Round().getPlayer2Movement(), is(EnumPlayerMovement.ROCK));
	}

	@Test
	public void shouldResetAllRounds() {
		// Given rounds
		List<Round> rounds = generateRandomRounds();

		// With some of them already reset
		rounds.get(0).setReset(true);
		rounds.get((int) NUMBER_OF_GENERATED_ROUNDS / 2).setReset(true);
		rounds.get((int) NUMBER_OF_GENERATED_ROUNDS - 1).setReset(true);

		// Execute
		List<Round> resetRounds = GameUtil.resetAllRounds(rounds);

		// Assert
		assertThat(resetRounds.stream().filter(Round::isReset).count(), is(NUMBER_OF_GENERATED_ROUNDS));

		List<Boolean> distinctValues = resetRounds.stream().map(Round::isReset).distinct().collect(Collectors.toList());

		assertThat(distinctValues.size(), is(1));
		assertTrue(distinctValues.get(0));
	}

	@Test
	public void shouldReturnEmptyListIfProvidedRoundIsNull() {
		// Execute
		List<Round> resetRounds = GameUtil.resetAllRounds(null);

		// Assert
		assertNotNull(resetRounds);
		assertTrue(resetRounds.isEmpty());

	}

	@Test
	public void shouldSecondPlayerFromGeneratedRoundAlwaysPlayRock() {
		// Given rounds
		List<Round> rounds = generateRandomRounds();

		// Assert second player always plays rock
		assertTrue(rounds.stream().map(Round::getPlayer2Movement).allMatch(EnumPlayerMovement.ROCK::equals));
	}

	@Test
	public void shouldRoundResultBeRight() {
		// Given rounds
		generateRandomRounds().forEach(round -> {
			// Assert
			assertThat(round.getRoundResult(), is(getRoundResult(round.getPlayer1Movement())));
		});
	}

	@Test
	public void shouldReturnEmptyReportWhenNullIsProvided() {
		// Execute
		GlobalGameReport globalGameReport = GameUtil.generateGlobalGameReport(null);

		// Assert
		assertThat(globalGameReport.getTotalDraws(), is(0L));
		assertThat(globalGameReport.getTotalRoundsPlayed(), is(0L));
		assertThat(globalGameReport.getTotalWinsForFirstPlayer(), is(0L));
		assertThat(globalGameReport.getTotalWinsForSecondPlayer(), is(0L));
	}

	@Test
	public void shouldReturnEmptyGlobalReportWhenEmptyIsProvided() {
		// Execute
		GlobalGameReport globalGameReport = GameUtil
				.generateGlobalGameReport(new ConcurrentHashMap<String, List<Round>>());

		// Assert
		assertThat(globalGameReport.getTotalDraws(), is(0L));
		assertThat(globalGameReport.getTotalRoundsPlayed(), is(0L));
		assertThat(globalGameReport.getTotalWinsForFirstPlayer(), is(0L));
		assertThat(globalGameReport.getTotalWinsForSecondPlayer(), is(0L));
	}

	@Test
	public void shouldReturnRightGlobalReportWhenNotEmptyListOfRoundsIsProvided() {
		// Given
		ConcurrentHashMap<String, List<Round>> games = new ConcurrentHashMap<String, List<Round>>();
		games.put(USER_1, generateRandomRounds());
		games.put(USER_2, generateRandomRounds());
		games.put(USER_3, generateRandomRounds());
		games.put(USER_4, generateRandomRounds());

		// Execute
		GlobalGameReport globalGameReport = GameUtil.generateGlobalGameReport(games);

		// Assert
		assertThat(globalGameReport.getTotalDraws() + globalGameReport.getTotalWinsForFirstPlayer()
				+ globalGameReport.getTotalWinsForSecondPlayer(), is(NUMBER_OF_GENERATED_ROUNDS * 4));
		assertThat(globalGameReport.getTotalRoundsPlayed(), is(NUMBER_OF_GENERATED_ROUNDS * 4));
	}

	@Test
	public void shouldReturnEmptyLocalReportWhenNullIsProvided() {
		// Execute
		LocalGameReport localGameReport = GameUtil.generateLocalGameReport(null, new User(USER_1));

		// Assert
		assertThat(localGameReport.getRoundFromCurrentGame().size(), is(0));
	}

	@Test
	public void shouldReturnEmptyLocalReportWhenEmptyIsProvided() {
		// Given
		ConcurrentHashMap<String, List<Round>> games = new ConcurrentHashMap<String, List<Round>>();

		// Execute
		LocalGameReport localGameReport = GameUtil.generateLocalGameReport(games, new User(USER_1));

		// Assert
		assertThat(localGameReport.getRoundFromCurrentGame().size(), is(0));
	}

	@Test
	public void shouldReturnRightLocalReportWhenWhenEmptyOrUnknownUserIsProvided() {
		// Given
		ConcurrentHashMap<String, List<Round>> games = new ConcurrentHashMap<String, List<Round>>();
		games.put(USER_1, generateRandomRounds());

		// Execute for unknown user
		LocalGameReport localGameReport = GameUtil.generateLocalGameReport(games, new User(USER_2));

		// Assert
		assertEquals(0, localGameReport.getRoundFromCurrentGame().size());

		// Execute for null user
		localGameReport = GameUtil.generateLocalGameReport(games, null);

		// Assert
		assertThat(localGameReport.getRoundFromCurrentGame().size(), is(0));
	}

	@Test
	public void shouldReturnRightLocalReportForNoneResetRound() {
		// Given
		ConcurrentHashMap<String, List<Round>> games = new ConcurrentHashMap<String, List<Round>>();
		games.put(USER_1, generateRandomRounds());
		games.put(USER_2, generateRandomRounds());
		games.put(USER_3, generateRandomRounds());
		games.put(USER_4, generateRandomRounds());

		// Execute for all non-reset rounds
		LocalGameReport localGameReport = GameUtil.generateLocalGameReport(games, new User(USER_1));

		// Assert
		assertThat(localGameReport.getRoundFromCurrentGame().size(), is((int) NUMBER_OF_GENERATED_ROUNDS));
	}

	@Test
	public void shouldReturnRightLocalReportForAllResetRounds() {
		// Given
		ConcurrentHashMap<String, List<Round>> games = new ConcurrentHashMap<String, List<Round>>();
		games.put(USER_1, generateRandomRounds());
		games.put(USER_2, generateRandomRounds());
		games.put(USER_3, generateRandomRounds());
		games.put(USER_4, generateRandomRounds());

		// Reset all the rounds
		games.computeIfPresent(USER_1, (key, rounds) -> GameUtil.resetAllRounds(rounds));

		// Execute for all reset rounds
		LocalGameReport localGameReport = GameUtil.generateLocalGameReport(games, new User(USER_1));

		// Assert
		assertThat(localGameReport.getRoundFromCurrentGame().size(), is(0));
	}

	@Test
	public void shouldReturnRightLocalReportForPartialResetRounds() {
		// Given
		ConcurrentHashMap<String, List<Round>> games = new ConcurrentHashMap<String, List<Round>>();
		games.put(USER_1, generateRandomRounds());
		games.put(USER_2, generateRandomRounds());
		games.put(USER_3, generateRandomRounds());
		games.put(USER_4, generateRandomRounds());

		// Reset all the rounds
		games.computeIfPresent(USER_1, (key, rounds) -> GameUtil.resetAllRounds(rounds));

		// Play some rounds
		games.computeIfPresent(USER_1, (key, rounds) -> {
			rounds.add(GameUtil.generateRound());
			rounds.add(GameUtil.generateRound());
			rounds.add(GameUtil.generateRound());
			return rounds;
		});

		// Execute for some rounds not reset
		LocalGameReport localGameReport = GameUtil.generateLocalGameReport(games, new User(USER_1));

		// Assert
		assertThat(localGameReport.getRoundFromCurrentGame().size(), is(3));
	}

	/**
	 * Current logic the game is using
	 * 
	 * @param playerMovement1
	 * @return
	 */
	private static EnumRoundResult getRoundResult(EnumPlayerMovement playerMovement1) {
		switch (playerMovement1) {
		case ROCK:
			return EnumRoundResult.DRAW;
		case SCISSORS:
			return EnumRoundResult.PLAYER_2_WIN;
		case PAPER:
			return EnumRoundResult.PLAYER_1_WIN;
		}

		return null;
	}

	/**
	 * Generate a bunch of rounds with round movements from player 1
	 * 
	 * @return
	 */
	private List<Round> generateRandomRounds() {
		return Stream.generate(GameUtil::generateRound).limit(NUMBER_OF_GENERATED_ROUNDS).collect(Collectors.toList());
	}

}
