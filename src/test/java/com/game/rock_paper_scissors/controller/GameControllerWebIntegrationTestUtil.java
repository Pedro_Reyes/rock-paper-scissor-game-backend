package com.game.rock_paper_scissors.controller;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.rock_paper_scissors.model.GlobalGameReport;
import com.game.rock_paper_scissors.model.LocalGameReport;
import com.game.rock_paper_scissors.model.User;

public class GameControllerWebIntegrationTestUtil {

	/**
	 * Map the String provided to a LocalGameReport object
	 * 
	 * @param result
	 * @return
	 */
	public static LocalGameReport resultToLocalGameReport(String result) {
		ObjectMapper mapper = new ObjectMapper();

		// JSON from String to Object
		LocalGameReport localGameReport = new LocalGameReport();

		try {
			localGameReport = mapper.readValue(result, LocalGameReport.class);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return localGameReport;
	}

	/**
	 * Map the String provided to a GlobalGameReport object
	 * 
	 * @param result
	 * @return
	 */
	public static GlobalGameReport resultToGlobalGameReport(String result) {
		ObjectMapper mapper = new ObjectMapper();

		// JSON from String to Object
		GlobalGameReport globalGameReport = new GlobalGameReport();

		try {
			globalGameReport = mapper.readValue(result, GlobalGameReport.class);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return globalGameReport;
	}
	
	/**
	 * Map the String provided to a GlobalGameReport object
	 * 
	 * @param result
	 * @return
	 */
	public static User resultToUserObject(String result) {
		ObjectMapper mapper = new ObjectMapper();

		// JSON from String to Object
		User user = null;

		try {
			user = mapper.readValue(result, User.class);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return user;
	}
	
}
