package com.game.rock_paper_scissors.controller;

import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.game.rock_paper_scissors.model.User;

/**
 * This class does not extensively checks everything from the controller as we
 * already do that in integration test and unit testing. The main purpose is
 * just showing how to test an call to the API REST.
 * 
 * "web integration test" impacts drastically the time for running test.
 * 
 * @author pedro
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GameControllerWebIntegrationTest {

	private static final String ENDPOINT_GENERATE_UID = "/game/generate-uid";

	@Autowired
	GameController gameController;

	@LocalServerPort
	private int port;
	TestRestTemplate restTemplate = new TestRestTemplate();
	HttpHeaders headers = new HttpHeaders();

	@Test
	public void shouldGenerateUIDUnique() throws Exception {
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> response1 = restTemplate.exchange(createURLWithPort(ENDPOINT_GENERATE_UID),
				HttpMethod.GET, entity, String.class);
		ResponseEntity<String> response2 = restTemplate.exchange(createURLWithPort(ENDPOINT_GENERATE_UID),
				HttpMethod.GET, entity, String.class);

		User user1 = GameControllerWebIntegrationTestUtil.resultToUserObject(response1.getBody());
		User user2 = GameControllerWebIntegrationTestUtil.resultToUserObject(response2.getBody());

		assertNotEquals(user1.getUid(), user2.getUid());
	}

	// TODO: Similar test should be created to be rigorous as we did for
	// GameUtilTest.java and GameSerivceiMplIntegrationTest.java

	/**
	 * Return the base URL for calling the API
	 * 
	 * @param uri
	 * @return
	 */
	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}
}
