package com.game.rock_paper_scissors.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.game.rock_paper_scissors.controller.GameControllerWebIntegrationTestUtil;
import com.game.rock_paper_scissors.model.GlobalGameReport;
import com.game.rock_paper_scissors.model.LocalGameReport;
import com.game.rock_paper_scissors.model.Round;
import com.game.rock_paper_scissors.model.User;

/**
 * "integration test" impacts drastically the time for running test
 * 
 * @author pedro
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class GameServiceImplIntegrationTest {

	private static User USER_1;
	private static User USER_2;
	private static User USER_3;
	private static User USER_4;

	private static final long NUMBER_OF_GENERATED_UID = 10;
	private static final int NUMBER_OF_GENERATED_RESTARTED_ROUNDS = 3;
	private static final int NUMBER_OF_GENERATED_NON_RESTARTED_ROUNDS = 2;

	@Autowired 
	GameServiceImpl gameServiceImpl;
	
	@Before
	public void setup() {
		USER_1 = new User();
		USER_1.setUid("1");
		
		USER_2 = new User();
		USER_2.setUid("2");
		
		USER_3 = new User();
		USER_3.setUid("3");
		
		USER_4 = new User();
		USER_4.setUid("4");
	}

	@Test
	public void shouldGenerateUIDNotRepeated() {
		// Given
		List<String> uids = Stream.generate(() -> {
			// Execute
			return gameServiceImpl.generateUID();
		}).limit(NUMBER_OF_GENERATED_UID).collect(Collectors.toList());

		// Assert
		assertThat(uids.stream().distinct().count(), is(NUMBER_OF_GENERATED_UID));
	}

	@Test
	public void shouldReportOnlyNonResetRounds() {
		// Adding some rounds
		gameServiceImpl.playRound(USER_1);
		gameServiceImpl.playRound(USER_1);
		gameServiceImpl.playRound(USER_1);
		gameServiceImpl.restartGame(USER_1);

		// Execute
		String result = gameServiceImpl.playRound(USER_1);

		LocalGameReport localGameReport = GameControllerWebIntegrationTestUtil.resultToLocalGameReport(result);

		// Assert
		List<Round> rounds = localGameReport.getRoundFromCurrentGame();

		List<Boolean> distinctValues = rounds.stream().map(Round::isReset).distinct().collect(Collectors.toList());

		assertThat(distinctValues.size(), is(1));
	}

	@Test
	public void shouldReportNoRoundsAfterRestart() {
		// Adding some rounds
		gameServiceImpl.playRound(USER_1);
		gameServiceImpl.playRound(USER_1);
		gameServiceImpl.playRound(USER_1);
		gameServiceImpl.restartGame(USER_1);

		// Execute
		String result = gameServiceImpl.restartGame(USER_1);

		LocalGameReport localGameReport = GameControllerWebIntegrationTestUtil.resultToLocalGameReport(result);

		// Assert
		List<Round> rounds = localGameReport.getRoundFromCurrentGame();
		assertThat(rounds.size(), is(0));
	}

	@Test
	public void shouldReportAllRoundsEvenThoseFromRestartedGames() {
		// Adding some rounds
		createUserWithRoundsAssociated(USER_1, NUMBER_OF_GENERATED_NON_RESTARTED_ROUNDS,
				NUMBER_OF_GENERATED_RESTARTED_ROUNDS);
		createUserWithRoundsAssociated(USER_2, NUMBER_OF_GENERATED_NON_RESTARTED_ROUNDS,
				NUMBER_OF_GENERATED_RESTARTED_ROUNDS);
		createUserWithRoundsAssociated(USER_3, NUMBER_OF_GENERATED_NON_RESTARTED_ROUNDS,
				NUMBER_OF_GENERATED_RESTARTED_ROUNDS);
		createUserWithRoundsAssociated(USER_4, NUMBER_OF_GENERATED_NON_RESTARTED_ROUNDS,
				NUMBER_OF_GENERATED_RESTARTED_ROUNDS);

		// Execute
		String result = gameServiceImpl.generateGlobalGameReport();

		GlobalGameReport globalGameReport = GameControllerWebIntegrationTestUtil.resultToGlobalGameReport(result);

		// Assert
		long totalNumberOfRounds = 4
				* (NUMBER_OF_GENERATED_NON_RESTARTED_ROUNDS + NUMBER_OF_GENERATED_RESTARTED_ROUNDS);
		assertThat(globalGameReport.getTotalRoundsPlayed(), is(totalNumberOfRounds));
		assertThat(globalGameReport.getTotalDraws() + globalGameReport.getTotalWinsForFirstPlayer()
				+ globalGameReport.getTotalWinsForSecondPlayer(), is(totalNumberOfRounds));
	}
	
	@Test
	public void shouldReportAddedRound() {
		// Adding some rounds
		gameServiceImpl.playRound(USER_1);
		gameServiceImpl.playRound(USER_1);
		gameServiceImpl.playRound(USER_1);
		

		// Execute
		String result = gameServiceImpl.playRound(USER_1);

		LocalGameReport localGameReport = GameControllerWebIntegrationTestUtil.resultToLocalGameReport(result);

		// Assert
		List<Round> rounds = localGameReport.getRoundFromCurrentGame();
		assertEquals(4, rounds.stream().filter(round -> !round.isReset()).count());

		List<Boolean> distinctValues = rounds.stream().map(Round::isReset).distinct().collect(Collectors.toList());

		assertThat(distinctValues.size(), is(1));
		assertFalse(distinctValues.get(0));
	}

	/**
	 * Create a user with rounds associated, some of these rounds are not part of
	 * the current game
	 * 
	 * @param user
	 */
	private void createUserWithRoundsAssociated(User user, int numberOfNonRestarted, int numberOfRestartedRounds) {
		// Rounds restarted
		IntStream.range(0, numberOfNonRestarted).forEach(n -> gameServiceImpl.playRound(user));
		gameServiceImpl.restartGame(user);

		// Rounds not restarted
		IntStream.range(0, numberOfRestartedRounds).forEach(n -> gameServiceImpl.playRound(user));
	}
}
