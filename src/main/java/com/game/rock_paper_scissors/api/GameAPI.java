package com.game.rock_paper_scissors.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.game.rock_paper_scissors.model.User;

public interface GameAPI {

	@RequestMapping(value = "/play-round", method = RequestMethod.POST, 
			produces = "application/json", consumes="application/json")
	public ResponseEntity<String> playRound(@RequestBody User user);

	@RequestMapping(value = "/restart", method = RequestMethod.POST)
	public ResponseEntity<String> restartGame(@RequestBody User user);

	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public ResponseEntity<String> gamesReport();

	@RequestMapping(value = "/generate-uid", method = RequestMethod.GET)
	public ResponseEntity<String> generateUID();

}
