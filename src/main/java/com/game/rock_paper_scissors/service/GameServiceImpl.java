package com.game.rock_paper_scissors.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import com.game.rock_paper_scissors.model.Round;
import com.game.rock_paper_scissors.model.User;
import com.game.rock_paper_scissors.util.GameUtil;
import com.game.rock_paper_scissors.util.JsonMapperUtil;

/**
 * In charge of the next operations:
 * 
 * <ul>
 * <li>Playing a round in a particular game of an specific user id</li>
 * <li>Restart game for a particular user id</li>
 * <li>Generate a global report of all the games played, even those
 * restarted</li>
 * <li>Generate an UID for new user coming for playing a game</li>
 * </ul>
 * 
 * @author pedro
 *
 */
@Service
public class GameServiceImpl implements IGameService {

	private static Map<String, List<Round>> games;
	
	GameServiceImpl(){
		games = new ConcurrentHashMap<String, List<Round>>();
	}

	/**
	 * Associates to the user ID given a new round.
	 * 
	 * Returns the scores taking only into account the current users rounds.
	 * 
	 * @param uid
	 * @return
	 */
	@Override
	public String playRound(User user) {
		if (games == null) {
			return null;
		}

		games.computeIfAbsent(user.getUid(), key -> new ArrayList<Round>());

		games.computeIfPresent(user.getUid(), (key, rounds) -> {
			rounds.add(GameUtil.generateRound());
			return rounds;
		});

		// Return report of current current rounds not reset
		return JsonMapperUtil.getJsonFromObject(GameUtil.generateLocalGameReport(games, user));
	}

	/**
	 * Current user id rounds are reset in order to be detected as round that are
	 * not part of the current game.
	 * 
	 * Returns the scores taking only into account the current users rounds.
	 * 
	 * @param uuid
	 * @return
	 */
	@Override
	public String restartGame(User user) {
		if (games == null) {
			return null;
		}

		// Reset all the rounds
		games.compute(user.getUid(), (key, rounds) -> {
			return GameUtil.resetAllRounds(rounds);
		});

		// Return report of current current rounds not reset
		return JsonMapperUtil.getJsonFromObject(GameUtil.generateLocalGameReport(games, user));
	}

	/**
	 * A report of all the rounds played is returned
	 * 
	 * @return
	 */
	@Override
	public String generateGlobalGameReport() {
		return JsonMapperUtil.getJsonFromObject(GameUtil.generateGlobalGameReport(games));
	}

	/**
	 * Generate a User ID for a particular game played by a user
	 * 
	 * @return
	 */
	@Override
	public String generateUID() {
		User user = new User();
		user.setUid(GameUtil.generateNewUID());
		return JsonMapperUtil.getJsonFromObject(user);
	}
}
