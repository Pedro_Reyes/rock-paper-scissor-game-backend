package com.game.rock_paper_scissors.model;


/**
 * Class for storing round information
 * 
 * @author pedro
 *
 */
public class Round {

	// A round reset means that the round is not part of the current game
	private boolean isReset;

	// Each player movement and the result of it
	private EnumPlayerMovement player1Movement;
	private EnumPlayerMovement player2Movement; // WARNING: do not apply modifier static final due to JSON-Object mapping
	private EnumRoundResult roundResult;
	
	public Round() {
		this.player2Movement = EnumPlayerMovement.ROCK;
	}

	public boolean isReset() {
		return isReset;
	}

	public void setReset(boolean isReset) {
		this.isReset = isReset;
	}

	public EnumPlayerMovement getPlayer1Movement() {
		return player1Movement;
	}

	public void setPlayer1Movement(EnumPlayerMovement player1Movement) {
		this.player1Movement = player1Movement;
	}

	public EnumPlayerMovement getPlayer2Movement() {
		return player2Movement;
	}
	
	public void setPlayer2Movement(EnumPlayerMovement player2Movement) {
		if(player2Movement!=EnumPlayerMovement.ROCK) {
			throw new IllegalArgumentException("Player 2 can only play ROCK");
		}
		
		this.player2Movement = player2Movement;
	}

	public EnumRoundResult getRoundResult() {
		return roundResult;
	}

	public void setRoundResult(EnumRoundResult roundResult) {
		this.roundResult = roundResult;
	}

}
