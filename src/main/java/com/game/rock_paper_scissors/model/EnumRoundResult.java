package com.game.rock_paper_scissors.model;

public enum EnumRoundResult {

	PLAYER_1_WIN, PLAYER_2_WIN, DRAW
	
}
