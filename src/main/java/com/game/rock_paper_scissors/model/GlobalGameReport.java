package com.game.rock_paper_scissors.model;

public class GlobalGameReport {

	private long totalRoundsPlayed;
	private long totalWinsForFirstPlayer;
	private long totalWinsForSecondPlayer;
	private long totalDraws;

	public long getTotalRoundsPlayed() {
		return totalRoundsPlayed;
	}

	public void setTotalRoundsPlayed(long totalRoundsPlayed) {
		this.totalRoundsPlayed = totalRoundsPlayed;
	}

	public long getTotalWinsForFirstPlayer() {
		return totalWinsForFirstPlayer;
	}

	public void setTotalWinsForFirstPlayer(long totalWinsForFirstPlayer) {
		this.totalWinsForFirstPlayer = totalWinsForFirstPlayer;
	}

	public long getTotalWinsForSecondPlayer() {
		return totalWinsForSecondPlayer;
	}

	public void setTotalWinsForSecondPlayer(long totalWinsForSecondPlayer) {
		this.totalWinsForSecondPlayer = totalWinsForSecondPlayer;
	}

	public long getTotalDraws() {
		return totalDraws;
	}

	public void setTotalDraws(long totalDraws) {
		this.totalDraws = totalDraws;
	}

}
