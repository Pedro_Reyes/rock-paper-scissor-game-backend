package com.game.rock_paper_scissors.util;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.game.rock_paper_scissors.model.GlobalGameReport;
import com.game.rock_paper_scissors.model.LocalGameReport;
import com.game.rock_paper_scissors.model.EnumPlayerMovement;
import com.game.rock_paper_scissors.model.Round;
import com.game.rock_paper_scissors.model.EnumRoundResult;
import com.game.rock_paper_scissors.model.User;

/**
 * Utility class for game general operations
 * 
 * @author pedro
 *
 */
public class GameUtil {

	private static final ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();

	/**
	 * Generate a new UID
	 * 
	 * @return
	 */
	public static String generateNewUID() {
		return String.valueOf(
				LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli() + threadLocalRandom.nextInt(1, 1_000_000));
	}

	/**
	 * Given a list of rounds, all of them will be reset so won't be part of current
	 * game played by the user
	 * 
	 * @param rounds
	 * @return
	 */
	public static List<Round> resetAllRounds(List<Round> rounds) {
		if (rounds == null) {
			return Collections.synchronizedList(new ArrayList<Round>());
		}

		rounds.forEach(round -> round.setReset(true));

		return rounds;
	}

	/**
	 * Generate a random round between player 1 and player 2 given the constant of
	 * player 2 playing always rock.
	 * 
	 * @return
	 */
	public static Round generateRound() {
		Round round = new Round();

		EnumPlayerMovement player1Movement = getRandomMovement();

		round.setPlayer1Movement(player1Movement);

		// Player 2 always plays ROCK
		
		switch (player1Movement) {
		case ROCK:
			round.setRoundResult(EnumRoundResult.DRAW);
			break;

		case SCISSORS:
			round.setRoundResult(EnumRoundResult.PLAYER_2_WIN);
			break;

		case PAPER:
			round.setRoundResult(EnumRoundResult.PLAYER_1_WIN);
			break;
		}

		return round;
	}

	/**
	 * A report of all the rounds played is returned
	 * 
	 * @return
	 */
	public static GlobalGameReport generateGlobalGameReport(Map<String, List<Round>> games) {
		if(games==null) {
			return new GlobalGameReport();
		}
		
		GlobalGameReport gameReport = new GlobalGameReport();

		gameReport.setTotalRoundsPlayed(GameUtil.countTotalRounds(games));
		gameReport.setTotalWinsForFirstPlayer(GameUtil.getTotalScore(games, EnumRoundResult.PLAYER_1_WIN));
		gameReport.setTotalWinsForSecondPlayer(GameUtil.getTotalScore(games, EnumRoundResult.PLAYER_2_WIN));
		gameReport.setTotalDraws(GameUtil.getTotalScore(games, EnumRoundResult.DRAW));

		return gameReport;
	}

	/**
	 * A report of all the non-reset rounds currently played by the user.
	 * 
	 * @return
	 */
	public static LocalGameReport generateLocalGameReport(Map<String, List<Round>> games, User user) {
		if (user == null || games == null) {
			return new LocalGameReport();
		}
		
		LocalGameReport localGameReport = new LocalGameReport();

		List<Round> userCurrentRounds = games.get(user.getUid());
		
		if(userCurrentRounds == null) {
			return new LocalGameReport();
		}

		userCurrentRounds = userCurrentRounds.stream()
				.filter(Objects::nonNull)
				.filter(isNotReset())
				.collect(Collectors.toList());

		localGameReport.setRoundFromCurrentGame(userCurrentRounds);

		return localGameReport;
	}

	/**
	 * Total score for any of the possible round results
	 * 
	 * @param roundResult
	 * @return
	 */
	private static long getTotalScore(Map<String, List<Round>> games, EnumRoundResult roundResult) {
		if (games == null) {
			return 0;
		}

		return games.values().stream()
				.filter(Objects::nonNull).flatMap(List::stream)
				.filter(Objects::nonNull).map(Round::getRoundResult)
				.filter(roundResult::equals).count();
	}

	/**
	 * Return a random movement
	 * 
	 * @return
	 */
	private static EnumPlayerMovement getRandomMovement() {
		EnumPlayerMovement player1Movement = EnumPlayerMovement.values()[threadLocalRandom.nextInt(0,
				EnumPlayerMovement.values().length)];
		return player1Movement;
	}
	
	/**
	 * Sweep over all the rounds of each user and count them to return the global sum
	 * @param games
	 * @return
	 */
	private static long countTotalRounds(Map<String, List<Round>> games) {
		return games.values().stream().flatMap(Stream::of).flatMap(List::stream).count();
	}

	private static Predicate<? super Round> isNotReset() {
		return isReset().negate();
	}

	private static Predicate<? super Round> isReset() {
		return Round::isReset;
	}
}
