package com.game.rock_paper_scissors.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.game.rock_paper_scissors.api.GameAPI;
import com.game.rock_paper_scissors.model.User;
import com.game.rock_paper_scissors.service.GameServiceImpl;

@RestController
@RequestMapping("game")
public class GameController implements GameAPI {

	@Autowired
	GameServiceImpl gameService;

	@Override
	public ResponseEntity<String> playRound(User user) {
		return new ResponseEntity<>(gameService.playRound(user), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> restartGame(User user) {
		return new ResponseEntity<>(gameService.restartGame(user), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> gamesReport() {
		return new ResponseEntity<>(gameService.generateGlobalGameReport(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> generateUID() {
		return new ResponseEntity<>(gameService.generateUID(), HttpStatus.OK);
	}

}
